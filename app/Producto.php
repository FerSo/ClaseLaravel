<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    public $table = 'productos';

    public $primaryKey = 'id';

    public $fillable = [
        'id',
        'nombre',
        'descripcion',
        'precio',
        'status',
        'cantidad',
        'created_at',
        'updated_at'

    ];
}
