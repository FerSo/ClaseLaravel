<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{

    public function obtener_todos(){
        $productos = Producto::all();
        return $productos;
    }

    public function todos(){
        $productos = self::obtener_todos();
        return view('productos')->with([
            'productos' => $productos
        ]);
    }

    public function nuevo(){

        return view('nuevoproducto');
    }
    public function nuevopost(Request $request){

//        return $request->all();

        $name = $request->get('nombre');
        $description = $request->get('descripcion');
        $precio = $request->get('precio');
        $cantidad = $request->get('cantidad');

        $producto = new Producto();
        $producto->nombre = $name;
        $producto->descripcion = $description;
        $producto->precio = $precio;
        $producto->cantidad = $cantidad;
        $producto->status = true;
        $producto->save();

        return redirect()->route('productos.todos');
    }
}
