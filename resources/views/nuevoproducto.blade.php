@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Nuevo Producto</div>
                    <div class="panel-body">
                        <form action="{{route('nuevo.producto.post')}}" method="post">
                            <div class="col-md-12"> <label for="nombre">Nombre</label>
                                <input type="text" required name="nombre" placeholder="inserte nombre" class="form-control" id="nombre">
                            </div>
                            <div class="col-md-12"> <label for="descripcion">Descripcion</label>
                                <textarea name="descripcion" required id="descripcion" form-control></textarea>
                            </div>
                            <div class="col-md-12"><label for="precio">Precio</label>
                                <input type="number" id="precio" required class="form-control" name="precio" placeholder="inserte precio">
                            </div>
                            <div class="col-md-12"><label for="cantidad">Cantidad</label>
                                <input type="number"  required placeholder="cantidad" name="cantidad" id="cantidad" class="form-control">
                            </div>
                            <div class="input-group"></div>

                            <button type="submit"  class="btn btn-success">Guardar</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
