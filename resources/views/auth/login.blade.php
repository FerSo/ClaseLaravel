<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clase Laravel / Login</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-reset.css')}}" rel="stylesheet">
    <link href="{{asset('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/style-responsive.css')}}" rel="stylesheet" />
</head>
<body class="login-body">
<div class="container">
    <form class="form-signin" method="POST" action="{{ route('login') }}">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <input type="email" name="email" class="form-control" placeholder="Email" autofocus>
            <input type="password" name="password" class="form-control" placeholder="Password">
            @if ($errors->has('email'))
                <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
            @if ($errors->has('password'))
                <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
            <button class="btn btn-lg btn-login btn-block" type="submit">Iniciar Sesión</button>
            <div class="registration">
                no tienes cuenta?
                <a class="" href="registration.html">
                    Crear Cuenta
                </a>
            </div>
        </div>
    </form>
</div>
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>

