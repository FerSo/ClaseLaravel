<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/productos',[
    'as' => 'productos.todos',
    'uses' => 'ProductoController@todos'
]);

// ejemplo de cambio

Route::get('/nuevo_producto',[
    'as' => 'nuevo.producto',
    'uses' => 'ProductoController@nuevo'
]);

Route::post('/nuevo_producto',[
    'as' => 'nuevo.producto.post',
    'uses' => 'ProductoController@nuevopost'
]);